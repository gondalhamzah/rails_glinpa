Rails.application.routes.draw do
  devise_for :admin, :controllers => { :registrations => 'admins/registrations' }
  resources :vehicle_models
  resources :vehicle_types
  resources :vehicle_brands
  resources :vehicles
  get 'add_preview/index/:id' => 'add_preview#index', as: 'add_preview'

  get 'dashboard/index' =>'dashboard#index', as: 'dashboard_path'

  get 'product/index'

  get 'product/new'

  get 'product/edit'

  get 'product/update'

  get 'product/destroy'

  get 'home_page/index'

  get 'ads/subcategories'

  root 'home_page#index'

  resources :services
  resources :feedbacks
  resources :emails
  resources :messages
  resources :comments
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :locations
  resources :bookings
  resources :conversations
  resources :categories
  resources :companies
  devise_for :users
  resources :listings
  resources :ads
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
