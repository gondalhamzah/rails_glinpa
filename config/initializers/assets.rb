# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Rails.application.config.assets.precompile += %w( all.js all.css )
Rails.application.config.assets.precompile += %w( jquery.js )
Rails.application.config.assets.precompile += %w( style.css )
Rails.application.config.assets.precompile += %w( bootstrap.css )
Rails.application.config.assets.precompile += %w( vendor.min.css )
Rails.application.config.assets.precompile += %w( bootstrap.js )
Rails.application.config.assets.precompile += %w( bootstrap.bundle.js )
Rails.application.config.assets.precompile += %w( vendor.min.js )
Rails.application.config.assets.precompile += %w( modernizr.min.js )
Rails.application.config.assets.precompile += %w( smart_wizard_theme_arrows.css )
Rails.application.config.assets.precompile += %w( smart_wizard.css )
Rails.application.config.assets.precompile += %w( jquery.smartWizard.js )

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admins.js admins.css )
