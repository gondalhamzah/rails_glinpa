require "rails_helper"

RSpec.describe "signing admin in" do
  it "succeeds with a valid email" do
    visit admin_session_path
    expect(page.body).to have_text("Email")
  end
end