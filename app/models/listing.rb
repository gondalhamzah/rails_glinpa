class Listing < ApplicationRecord
  belongs_to :user
  belongs_to :category
  belongs_to :company
  belongs_to :booking, optional: true
  has_many :messages
  has_many :locations
  # has_many :transactions
  has_many :comments
end
