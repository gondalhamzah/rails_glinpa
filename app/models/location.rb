class Location < ApplicationRecord
  belongs_to :listing
  belongs_to :service
  belongs_to :user
  belongs_to :company
end
