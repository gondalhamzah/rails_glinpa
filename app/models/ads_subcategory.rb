class AdsSubcategory < ApplicationRecord
  belongs_to :category
  has_many :ads
end
