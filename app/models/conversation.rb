class Conversation < ApplicationRecord
  belongs_to :user
  belongs_to :company
  # has_many :transactions
  has_many :messages

end
