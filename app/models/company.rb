class Company < ApplicationRecord
  has_many :listings
  has_many :conversations
  has_many :emails
  has_many :location
  has_many :feedbacks
end
