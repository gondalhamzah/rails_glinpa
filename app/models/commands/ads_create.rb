module Commands
  class AdsCreate
    attr_accessor :name, :description, :feature_image, :availability, :address, :quantity, :currency,
                  :price, :status, :category_id, :ads_subcategory_id, :current_user

    include ActiveModel::Model

    def initialize(current_user, attrs)
      @current_user = current_user
      super(attrs)
    end

    def execute
      return unless valid?
      # byebug
      @current_user.ads.create(
        name: name,
        description: description,
        feature_image: feature_image,
        availability: cast(availability),
        address: address,
        quantity: quantity,
        currency: currency,
        price: price,
        status: status,
        ads_subcategory_id: ads_subcategory_id
      )
    end

    private

    def cast(val)
      !!ActiveRecord::Type::Boolean.new.cast(val)
    end
  end
end