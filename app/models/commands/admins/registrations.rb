module Commands
  module Admins
    class Registrations
      attr_accessor :email, :password, :password_confirmation, :current_sign_in_ip,
                    :last_sign_in_ip, :current_admin

      include ActiveModel::Model

      validates :email, presence: true
      validates :password, presence: true
      validates :password, confirmation: { case_sensitive: true }

      validate :unique_email?

      def initialize(current_admin, attrs)
        @current_admin = current_admin
        super(attrs)
      end

      def execute
        return unless valid?
        admin = Admin.find_by_id(current_admin)

        if admin.email == current_admin.email
          Admin.transaction do
            admin_user = create_admin
            raise ActiveRecord::Rollback unless admin_user.valid?
            true
          end
        else
          false
        end
      end

      private

      def create_admin
        Admin.create(
          email: email,
          password: password,
          current_sign_in_ip: current_sign_in_ip,
          last_sign_in_ip: last_sign_in_ip
        )
      end

      def unique_email?
        errors.add(:email, "already taken") if Admin.where(email: email).exists?
      end
    end
  end
end