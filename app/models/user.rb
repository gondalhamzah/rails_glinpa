class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :listings
  has_many :conversations
  has_one  :location
  has_many :emails
  has_many :feedbacks
  has_many :services
  has_many :ads

end
