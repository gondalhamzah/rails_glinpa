class Feedback < ApplicationRecord
  belongs_to :user
  has_many :emails
  belongs_to :company
end
