class Service < ApplicationRecord
  belongs_to :booking
  has_many :comments
  belongs_to :user
  belongs_to :category
  # has_many :transactions
  has_many :locations

end
