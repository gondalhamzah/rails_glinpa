class Email < ApplicationRecord
  belongs_to :user
  belongs_to :feedback
  belongs_to :company
end
