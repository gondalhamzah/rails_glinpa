class Ad < ApplicationRecord
  belongs_to :ads_subcategory
  belongs_to :user, inverse_of: :ads
end
