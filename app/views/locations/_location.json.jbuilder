json.extract! location, :id, :latitude, :longitude, :address, :google_addres, :location_type, :created_at, :updated_at
json.url location_url(location, format: :json)
