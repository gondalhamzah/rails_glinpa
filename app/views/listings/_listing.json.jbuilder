json.extract! listing, :id, :title, :price, :description, :feature_image, :image_gallery, :availability, :address, :quantity, :currency, :created_at, :updated_at
json.url listing_url(listing, format: :json)
