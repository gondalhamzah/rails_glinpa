json.extract! service, :id, :title, :descripion, :feature_image, :image_gallery, :price, :address, :availability, :currency, :rating, :created_at, :updated_at
json.url service_url(service, format: :json)
