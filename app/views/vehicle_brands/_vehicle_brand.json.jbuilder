json.extract! vehicle_brand, :id, :name, :vehicle_type_id, :created_at, :updated_at
json.url vehicle_brand_url(vehicle_brand, format: :json)
