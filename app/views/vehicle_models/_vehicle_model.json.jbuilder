json.extract! vehicle_model, :id, :name, :created_at, :updated_at
json.url vehicle_model_url(vehicle_model, format: :json)
