json.extract! feedback, :id, :url, :comment, :created_at, :updated_at
json.url feedback_url(feedback, format: :json)
