class VehiclesController < ApplicationController
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!, only: [:index]
  # GET /vehicles
  # GET /vehicles.json
  def index
    @vehicles = Vehicle.all
  end

  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
  end

  # GET /vehicles/new
  def new
    @vehicle = Vehicle.new
  end

  # GET /vehicles/1/edit
  def edit
  end

  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle = Vehicle.new(vehicle_params)

    respond_to do |format|
      if @vehicle.save
        format.html { redirect_to @vehicle, notice: 'Vehicle was successfully created.' }
        format.json { render :show, status: :created, location: @vehicle }
      else
        format.html { render :new }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vehicles/1
  # PATCH/PUT /vehicles/1.json
  def update
    respond_to do |format|
      if @vehicle.update(vehicle_params)
        format.html { redirect_to @vehicle, notice: 'Vehicle was successfully updated.' }
        format.json { render :show, status: :ok, location: @vehicle }
      else
        format.html { render :edit }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    @vehicle.destroy
    respond_to do |format|
      format.html { redirect_to vehicles_url, notice: 'Vehicle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.require(:vehicle).permit(:condition, :carMaker, :carModel,
        :carType,
        :price, :gasType, :featureImag, :galleryImag, :internalColor,
        :externalColor,
        :transmision, :carYear, :carMileage, :carDoor, :engineType, :VIM,
        :previewOwner,
        :mapLocation, :seatsLine, :seatsMaterial, :airbag, :sportSeats,
        :electricalChairs,
        :supportFrontArms, :safetyBelt, :multifunctionGuide, :ac, :radio,
        :DVD, :cdPlayer,
        :cdBox, :usbinterface, :mp3Aux, :speakrsHifi, :profecionalSound,
        :wifi, :bluethooth,
        :trailerCouplig, :rearSpoiler, :halogenXenon, :fogLigth,
        :daytimeRunningLigths,
        :cleanRearGlasses, :electricMirrors, :electricDoors, :newRubbers,
        :factoryRings,
        :factoryPaints, :panoramicCeiling, :sunroof, :version, :absBrakes,
        :alarm, :reverseCamera, :rainSensor, :parkingSensor, :gps, :smartKey,
        :creuiseControl,
        :four_x_four, :turbo, :fullPower, :ecoSystem)
    end
end
