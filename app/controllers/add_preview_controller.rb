class AddPreviewController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

  def index
    @preview = Listing.find(params[:id])
  end
end
