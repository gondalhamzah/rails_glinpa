class HomePageController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

  def index
    @add = Listing.where(availability: true).order('currency DESC')
  end
end
