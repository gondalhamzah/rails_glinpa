class AdsController < ApplicationController

  def new
    @ad = Commands::AdsCreate.new(current_user, {})
  end

  def subcategories
    @response = AdsSubcategory.where(category_id: params[:category_id]).all
    render json: @response
  end

  def index
    @ads = Ad.all
  end

  def create
    @ad = Commands::AdsCreate.new(current_user, ads_params.to_h)
    if @ad.execute
      redirect_to ads_path
    else
      redirect_to new_ad_path
    end
  end

  private

  def ads_params
    params.require(:commands_ads_create).permit(
       :name,
       :description,
       :feature_image,
       :availability,
       :address,
       :quantity,
       :currency,
       :price,
       :status,
       :category_id,
       :ads_subcategory_id
    )
  end
end