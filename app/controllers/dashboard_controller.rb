class DashboardController < ApplicationController
  def index
    @add = Listing.where(user_id: current_user).order('currency DESC')
  end
end
