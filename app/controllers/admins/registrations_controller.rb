module Admins
  class RegistrationsController < Devise::RegistrationsController
    skip_before_action :require_no_authentication

    def new
      @command = Commands::Admins::Registrations.new(nil, {})
    end

    def create
      @command = Commands::Admins::Registrations.new(current_admin, new_admin_params.merge(
          current_sign_in_ip: request.remote_ip, last_sign_in_ip: request.remote_ip
      ).to_h)

      if @command.execute
        flash[:success] = "new admin user successfully created"
        redirect_to "/admin"
      else
        redirect_to new_admin_registration_path
      end
    end

    private

    def redirect_unless_admin
      unless current_admin
        flash[:error] = "Only admins can do that"
        redirect_to root_path
      end
    end

    def new_admin_params
      params.require(:command).permit(
        :email,
        :password,
        :password_confirmation,
      )
    end
  end
end
