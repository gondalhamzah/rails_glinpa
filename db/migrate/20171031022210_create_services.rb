class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :title
      t.text :description
      t.string :feature_image
      t.string :image_gallery
      t.string :price
      t.text :address
      t.boolean :availability
      t.integer :currency
      t.integer :rating
      t.integer :user_id
      t.integer :booking_id
      t.integer :category_id

      t.timestamps
    end
  end
end
