class CreateAds < ActiveRecord::Migration[5.1]
  def change
    create_table :ads do |t|
      t.string :status
      t.text :name
      t.text :description
      t.string :version
      t.string :version_id
      t.references :ads_subcategory, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

