class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :website
      t.text :address
      t.string :phone_number
      t.string :company_type
      t.string :fbpage
      t.string :twitter
      t.string :employees
      t.integer :rating

      t.timestamps
    end
  end
end
