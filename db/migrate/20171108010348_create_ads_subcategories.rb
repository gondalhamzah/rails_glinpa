class CreateAdsSubcategories < ActiveRecord::Migration[5.1]
  def change
    create_table :ads_subcategories do |t|
      t.text :name
      t.text :description
      t.boolean :enabled
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
