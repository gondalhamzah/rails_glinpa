class AddDataColumnsToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :price, :integer
    add_column :ads, :feature_image, :text
    add_column :ads, :availability, :boolean
    add_column :ads, :address, :text
    add_column :ads, :quantity, :integer
    add_column :ads, :currency, :text
  end
end
