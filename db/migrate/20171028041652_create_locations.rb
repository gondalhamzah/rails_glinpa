class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.float :latitude
      t.float :longitude
      t.text :address
      t.float :google_addres
      t.string :location_type
      t.integer :user_id
      t.integer :company_id
      t.integer :listing_id
      t.integer :service_id

      t.timestamps
    end
  end
end
