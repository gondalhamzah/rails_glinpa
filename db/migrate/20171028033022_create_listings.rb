class CreateListings < ActiveRecord::Migration[5.1]
  def change
    create_table :listings do |t|
      t.string :title
      t.integer :price
      t.text :description
      t.string :feature_image
      t.string :image_gallery
      t.boolean :availability
      t.text :address
      t.integer :quantity
      t.float :currency
      t.integer :user_id
      t.integer :booking_id
      t.integer :category_id
      t.integer :company_id

      t.timestamps
    end
  end
end
