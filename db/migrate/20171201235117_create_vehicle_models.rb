class CreateVehicleModels < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicle_models do |t|
      t.string :name
      t.integer :vehicle_brand_id

      t.timestamps
    end
  end
end
