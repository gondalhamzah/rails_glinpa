class CreateConversations < ActiveRecord::Migration[5.1]
  def change
    create_table :conversations do |t|
      t.string :title
      t.text :message
      t.integer :user_id
      t.integer :company_id

      t.timestamps
    end
  end
end
