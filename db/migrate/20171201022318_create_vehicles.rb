class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.string :condition
      t.string :carMaker
      t.string :carModel
      t.string :carType
      t.string :price
      t.string :gasType
      t.string :featureImag
      t.string :galleryImag
      t.string :internalColor
      t.string :externalColor
      t.string :transmision
      t.string :carYear
      t.string :carMileage
      t.string :carDoor
      t.string :engineType
      t.string :VIM
      t.integer :previewOwner
      t.string :mapLocation
      t.integer :seatsLine
      t.string :seatsMaterial
      t.boolean :airbag
      t.boolean :sportSeats
      t.boolean :electricalChairs
      t.boolean :supportFrontArms
      t.boolean :safetyBelt
      t.boolean :multifunctionGuide
      t.boolean :ac
      t.boolean :radio
      t.boolean :DVD
      t.boolean :cdPlayer
      t.boolean :cdBox
      t.boolean :usbinterface
      t.boolean :mp3Aux
      t.boolean :speakrsHifi
      t.boolean :profecionalSound
      t.boolean :wifi
      t.boolean :bluethooth
      t.boolean :trailerCouplig
      t.boolean :rearSpoiler
      t.boolean :halogenXenon
      t.boolean :fogLigth
      t.boolean :daytimeRunningLigths
      t.boolean :cleanRearGlasses
      t.boolean :electricMirrors
      t.boolean :electricDoors
      t.boolean :newRubbers
      t.boolean :factoryRings
      t.boolean :factoryPaints
      t.boolean :panoramicCeiling
      t.boolean :sunroof
      t.string :version
      t.integer :version_id
      t.boolean :absBrakes
      t.boolean :alarm
      t.boolean :reverseCamera
      t.boolean :rainSensor
      t.boolean :parkingSensor
      t.boolean :gps
      t.boolean :smartKey
      t.boolean :cruiseControl
      t.boolean :four_x_four
      t.boolean :turbo
      t.boolean :fullPower
      t.boolean :ecoSystem

      t.timestamps
    end
  end
end
