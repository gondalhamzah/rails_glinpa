class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.string :url
      t.text :comment
      t.integer :user_id
      t.integer :company_id

      t.timestamps
    end
  end
end
