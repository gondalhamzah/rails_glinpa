class CreateEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :emails do |t|
      t.string :address
      t.integer :user_id
      t.integer :company_id
      t.integer :feedback_id

      t.timestamps
    end
  end
end
