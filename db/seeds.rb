# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create([{
  email:'dbjj20@gmail.com',
  name:'dylan',
  last_name:'baez',
  password:'123456',
  address:'vive en la capital, eso es lejos',
  phone_number:'8092531630',
  date:'December 06, 2017'
}])

User.create([{
  email:'jorge@innova-code.com',
  name:'Jorge',
  last_name:'Dominguez',
  password:'123456',
  address:'Agustin Guerrero esquina Santome',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'jeisy@innova-code.com',
  name:'Jeisy',
  last_name:'Camacho',
  password:'123456',
  address:'Agustin Guerrero esquina Santome',
  phone_number:'809-639-8677',
  date:'December 06, 2017'
}])

User.create([{
  email:'Israe@innova-code.com',
  name:'Israel',
  last_name:'Castro',
  password:'123456',
  address:'Villa Cristal',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'Elizabeth@innova-code.com',
  name:'Elizabeth',
  last_name:'Amparo',
  password:'123456',
  address:'El Seibo',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'Breilin@innova-code.com',
  name:'Breilin',
  last_name:'Cedeño',
  password:'123456',
  address:'Villa Tropical',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'Elianna@innova-code.com',
  name:'Elianna',
  last_name:'Dominguez',
  password:'123456',
  address:'La Romana',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'Elvis@innova-code.com',
  name:'Elvis',
  last_name:'Ozoria',
  password:'123456',
  address:'Agustin Guerrero esquina Santome',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'Jeffrey@innova-code.com',
  name:'Jeffrey',
  last_name:'Santana',
  password:'123456',
  address:'Agustin Guerrero esquina Santome',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

User.create([{
  email:'Rafael@innova-code.com',
  name:'Rafael',
  last_name:'Perez',
  password:'123456',
  address:'Agustin Guerrero esquina Santome',
  phone_number:'809-673-0758',
  date:'December 06, 2017'
}])

Booking.create([{
  appoiment: Date.new()
}])

Category.create([{
    name:'Auto',
    description:'All type of vehicle goes in this place.'
}])

Category.create([{
    name:'Real Estate',
    description:'Apartament, Houses, Offices, all here.'
}])

Category.create([{
    name:'Services',
    description:'gardener, plumber, mechanic, makeup artist, artists, chef, hire the service you need the day you need.'
}])

Category.create([{
    name:'Items',
    description:'Buy fast or sale fast your choice.'
}])

Company.create([{
  name:'innovacode',
  website:'www.innova-code.com.do',
  address:'establecida en higuey',
  phone_number:'8098098090',
  company_type:'desarrollo de software',
  employees:'2'
}])

Conversation.create([{
  title:'todo que sube baja',
  message:'nada puede enfrentar la gravedad',
  user_id:1,
  company_id:1
}])

Conversation.create([{
  title:'todo que sube baja',
  message:'nada puede enfrentar la gravedad',
  user_id:2,
  company_id:1
}])

Conversation.create([{
  title:'todo que sube baja',
  message:'nada puede enfrentar la gravedad',
  user_id:3,
  company_id:1
}])

Conversation.create([{
  title:'todo que sube baja',
  message:'nada puede enfrentar la gravedad',
  user_id:4,
  company_id:1
}])

Conversation.create([{
  title:'todo que sube baja',
  message:'nada puede enfrentar la gravedad',
  user_id:5,
  company_id:1
}])

Conversation.create([{
    title:'todo que sube baja',
    message:'nada puede enfrentar la gravedad',
    user_id:6,
    company_id:1
}])

Conversation.create([{
    title:'todo que sube baja',
    message:'nada puede enfrentar la gravedad',
    user_id:7,
    company_id:1
}])

Feedback.create([{
  url:'www.google.com',
  comment:'esta pagina es de google',
  user_id:8,
  company_id:1
}])

Email.create([{
  address:'dbjj20@gmail.com',
  user_id:1,
  company_id:1,
  feedback_id:1
}])

Listing.create([{
  title:'2012 Porsche 911 Turbo',
  price:119785,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.',
  feature_image:'http://www.blogcdn.com/www.autoblog.com/media/2011/06/000-2012-porsche-911-918.jpg',
  image_gallery:'esto es image_gallery',
  availability:true,
  address:'Florida, United States',
  quantity:1,
  currency:4,
  user_id:1,
  booking_id:1,
  company_id:1,
  category_id:1
}])

Listing.create([{
  title:'Audi Q7 2016', price:83900,
  description:'Bajo el cofre encontramos el V6 3.0 litros de inducción forzada, entrega 333 hp y 325 lb-pie de torque y se asocia a una transmisión automática de ocho velocidades que envía la potencia a las cuatro ruedas vía el conocido sistema de tracción integral quattro. Lo anterior significa que el Q7 2016 es capaz de hacer un 0 – 100 km/h en apenas 6.3 segundos de acuerdo con las cifras oficiales.',
  feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1347/homeslide/59c9c69833f23.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, República Dominicana', quantity:1, currency:4,
  user_id:2,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'Mercedez-Benz GLE 450 2016',
  price:84400,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1141/main/59a5f7c2a7aed.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:3,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'Chevrolet Tahoe LTZ 2015',
  price:53500,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1449/homeslide/59db8d429f512.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:4,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'Mercedez-Benz Clase GLE 43 AM', price:107000,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1203/main/59b76c0848f2a.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:5,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'Volvo XC60 2018', price:119785,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1517/main/59ef5a521c361.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:6,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
    title:'Toyota 4Runner Limited 2017', price:58000,
    description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1564/main/59f8b51d1f967.jpg', image_gallery:'esto es image_gallery',
    availability:true, address:'Florida, United States', quantity:1, currency:4,
    user_id:7,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'Mercedes-Benz CLA45 AMG EDITION ONE 2015', price:41900,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1339/main/59c9ad30b89f9.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:8,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'BMW M4 2016', price:98000,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1511/main/59ee6acc3ca3c.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:9,booking_id:1,company_id:1,category_id:1
}])

Listing.create([{
  title:'Volvo V40 T4R 2018', price:119785,
  description:'Porsche certified warranty until July 2, 2018! This 2012 911 Turbo coupe is a one owner vehicle with a clean CarFax that was traded here at Hennessy Porsche. The original MSRP is $146,305. This 2012 911 Turbo is Black on Black full leather interior. Options include: heated seats, heated steering wheel, 19 RS spyder wheels, manual transmission, ventilated seats, Bi-Xenon lights with dynamic lighting system, XM radio, door entry guards in carbon fiber and Porsche crests embossed on headrests.', feature_image:'https://www.autogestores.com/wp-content/uploads/thememakers/cardealer/2/1505/main/59ee4fd13652c.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Florida, United States', quantity:1, currency:4,
  user_id:10,booking_id:1,company_id:1,category_id:1
}])
Listing.create([{
  title:'Apartamento nuevo 1er piso en Gazcue', price:5600000,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/81/8158966256.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:1,booking_id:1,company_id:1,category_id:2
}])

Listing.create([{
  title:'Torre en planos alma rosa primera', price:7900000,
  description:'Características generales: 3 habitaciones, Habitación Principal con su baño y vestidor, Baño de visitas
  Sala, Sala de estar, Comedor, Cocina, Balcon, Habitación de servicio con su baño, Área de lavado, 2 Parqueos, Madera de roble
  Pisos en porcelanato, Topé de cocina de granito natural, Techo y cornisa en yeso, Pozo tubular, Pre instalación de a/c, Lobby,
  Gimnasio,Portón electrico, Intercom, Planta full 24h, Gas comun, Cámaras de vigilancia.',
  feature_image:'http://content.corotos.com.do/pictures/81/8158966256.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:2,booking_id:1,company_id:1,category_id:2
}])

Listing.create([{
  title:'Edificio empresarial proximo Av. Independencia', price:45000000,
  description:'Edificio empresarial en Gazcue ideal para call centers, consorcios de banca, instituciones educativas, centros médicos, entre otros.',
  feature_image:'http://content.corotos.com.do/pictures/80/8068756834.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:3,booking_id:1,company_id:1,category_id:2
}])
Listing.create([
  {title:'Locales Comerciales', price:244000,
  description:'Locales comerciales para Restaurantes, Bancos, Farmacia, Bares, Minimarket, Otros.',
  feature_image:'http://content.corotos.com.do/pictures/80/8070626277.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:4,booking_id:1,company_id:1,category_id:2}])
  Listing.create([
  {title:'Penthouse 400 mts2 en Evaristo Morales', price:320000,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/81/8150651225.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:5,booking_id:1,company_id:1,category_id:2}])
  Listing.create([
  {title:'Penthouse en Naco - 4 Habitaciones - 5 Parqueos', price:380000,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/80/8093754520.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:6,booking_id:1,company_id:1,category_id:2}])
  Listing.create([
  {title:'Pent House en Colinas de los Rios', price:4800000,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/80/8036446388.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:7,booking_id:1,company_id:1,category_id:2}])

  Listing.create([{title:'Nuevo a estrenar ph en los cacicazgos 320m', price:330000,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/79/7929523417.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:8,booking_id:1,company_id:1,category_id:2}])

  Listing.create([{title:'Bella Vista PH 470mts', price:349000,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/80/8012057077.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:9,booking_id:1,company_id:1,category_id:2}])

  Listing.create([{title:'Proyecto en NACO Penthouse 3hab 615m2', price:816870,
  description:'Se vende hermoso apartamento de 3 habitaciones en Gazcue. Excelentes condiciones, como nuevo, poco tiempo de uso.',
  feature_image:'http://content.corotos.com.do/pictures/80/8051039972.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:10,booking_id:1,company_id:1,category_id:2}])

  Listing.create([{title:'iPhone X', price:1500, description:'iPhone X 2017.',
  feature_image:'https://cdn.macrumors.com/article-new/2017/09/iphonexdesign.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:1,booking_id:1,company_id:1,category_id:4}])
  Listing.create([
  {title:'Apple Watch Series', price:309, description:'Apple Watch.',
  feature_image:'https://i.ebayimg.com/images/g/UWAAAOSwHYpZ75K5/s-l500.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:2,booking_id:1,company_id:1,category_id:4}])
  Listing.create([
  {title:'Xbox One X', price:500, description:'Xbox One X.',
  feature_image:'https://cdn.wccftech.com/wp-content/uploads/2017/09/Xbox-One-X.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:3,booking_id:1,company_id:1,category_id:4}])
  Listing.create([
  {title:'VANS AUTHENTIC CLASSIC SNIKERS', price:34.99, description:'Xbox One X.',
  feature_image:'https://shop.r10s.jp/sneaker-bouz/cabinet/vn0ee3blk.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:4,booking_id:1,company_id:1,category_id:4}])
  Listing.create([
  {title:'Echo Dot (2nd Generation) - Black', price:49.99, description:'Amazon Dots.',
  feature_image:'https://images-na.ssl-images-amazon.com/images/I/51sCIxg4zrL._SY300_.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:5,booking_id:1,company_id:1,category_id:4}])
  Listing.create([
  {title:'Intel BOXDP67BGB3', price:199.99, description:'Intel BOXDP67BGB3.',
  feature_image:'https://images-na.ssl-images-amazon.com/images/I/51QJS4aYCgL.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:6,booking_id:1,company_id:1,category_id:4}])
  Listing.create([{title:'GIGABYTE GA-H110M-A', price:46.99, description:'Intel BOXDP67BGB3.',
  feature_image:'https://images-na.ssl-images-amazon.com/images/I/71l1fflbmbL._SL1000_.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:7,booking_id:1,company_id:1,category_id:4}])
  Listing.create([{title:'Fire 7', price:49.99, description:'Intel BOXDP67BGB3.',
  feature_image:'https://images-na.ssl-images-amazon.com/images/I/71KrKaf3hVL._SL1000_.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:8,booking_id:1,company_id:1,category_id:4}])
  Listing.create([{title:'Ssyiz Custom Womens Vintage', price:42.99, description:'Intel BOXDP67BGB3.',
  feature_image:'https://images-na.ssl-images-amazon.com/images/I/612guDlJLzL._UY445_.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:9,booking_id:1,company_id:1,category_id:4}])
  Listing.create([{title:'POGT Women 3/4', price:23.99, description:'Intel BOXDP67BGB3.',
  feature_image:'https://images-na.ssl-images-amazon.com/images/I/61lutNmWk7L._UY445_.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', quantity:1, currency:4,
  user_id:10,booking_id:1,company_id:1,category_id:4
  }])
Message.create([{
  content:'content of the message',
  conversation_id:1,
  listing_id:1,
  user_id:1
  }])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:2,
user_id:2
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:3,
user_id:3
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:4,
user_id:4
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:5,
user_id:5
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:6,
user_id:6
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:7,
user_id:7
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:8,
user_id:8
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:9,
user_id:9
}])
Message.create([{
content:'content of the message',
conversation_id:1,
listing_id:10,
user_id:10
}])
Service.create([{
  title:'CYM COMPUTER, S.R.L', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'http://media.prleap.com/image/755/full/contentmotion_-_no_strap.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3}])

  Service.create([{title:'Mobile Shop', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://s3.envato.com/files/142910109/prev-banner.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Digital Coupons Store', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'http://www.simplygood.com/wp-content/uploads/2012/01/sg_banner_store.png', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Celulares H & G', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/23664740-1574-40ff-a691-fd2b47795b45.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Joyeria Soler by Hilda Soler', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/c8891c03-ca66-4ffc-b307-dd487455e433.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Mega Supply', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/6a4d6d69-f0c6-4b1f-b4ab-7a9a8597081e.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Tienda Click 2', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/54e732bc-9eae-4bd5-bbbf-5167e76688af.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'OAS Computer', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/0c79b5e4-ffc0-4708-8b6a-972b9664b32d.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Elite Brokers', price:0,
  description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/e142cef8-ae2d-4df0-b386-819fa45fd780.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4}])

  Service.create([
  {title:'Mundo Fibra', price:0,description:'Somos una empresa legalmente constituida desde 2011, ofrecemos a nuestros clientes tecnología de punta, tal como laptops, celulares, computadores, accesorios y soluciones corporativas. Somos importadores de diferentes marcas como DELL, SAMSUNG, ARGOM, IMEXX, APPLE, HTC, ALCATEL, KIVO, entre otras.',
  feature_image:'https://cdn-images.schibsted.com/corotos-pre/images/shops/36ffe3d8-4a98-45b6-aa14-3b36dc114c78.jpg', image_gallery:'esto es image_gallery',
  availability:true, address:'Santo Domingo, Republica Dominicana', currency:4,
  user_id:1,booking_id:1,category_id:3,rating:4
  }])

  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])

  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
  Comment.create([{
      content:'comentario',
      listing_id:1,
      user_id:1,
      service_id:1
  }])
