# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171208041530) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ads", force: :cascade do |t|
    t.string "status"
    t.text "name"
    t.text "description"
    t.string "version"
    t.string "version_id"
    t.bigint "ads_subcategory_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "price"
    t.text "feature_image"
    t.boolean "availability"
    t.text "address"
    t.integer "quantity"
    t.text "currency"
    t.index ["ads_subcategory_id"], name: "index_ads_on_ads_subcategory_id"
    t.index ["user_id"], name: "index_ads_on_user_id"
  end

  create_table "ads_subcategories", force: :cascade do |t|
    t.text "name"
    t.text "description"
    t.boolean "enabled"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_ads_subcategories_on_category_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.date "appoiment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.integer "listing_id"
    t.integer "user_id"
    t.integer "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.text "address"
    t.string "phone_number"
    t.string "company_type"
    t.string "fbpage"
    t.string "twitter"
    t.string "employees"
    t.integer "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.string "title"
    t.text "message"
    t.integer "user_id"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emails", force: :cascade do |t|
    t.string "address"
    t.integer "user_id"
    t.integer "company_id"
    t.integer "feedback_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "url"
    t.text "comment"
    t.integer "user_id"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "listings", force: :cascade do |t|
    t.string "title"
    t.integer "price"
    t.text "description"
    t.string "feature_image"
    t.string "image_gallery"
    t.boolean "availability"
    t.text "address"
    t.integer "quantity"
    t.float "currency"
    t.integer "user_id"
    t.integer "booking_id"
    t.integer "category_id"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.float "latitude"
    t.float "longitude"
    t.text "address"
    t.float "google_addres"
    t.string "location_type"
    t.integer "user_id"
    t.integer "company_id"
    t.integer "listing_id"
    t.integer "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text "content"
    t.integer "conversation_id"
    t.integer "listing_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "feature_image"
    t.string "image_gallery"
    t.string "price"
    t.text "address"
    t.boolean "availability"
    t.integer "currency"
    t.integer "rating"
    t.integer "user_id"
    t.integer "booking_id"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "name"
    t.string "last_name"
    t.text "address"
    t.string "phone_number"
    t.integer "rating"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vehicle_brands", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vehicle_models", force: :cascade do |t|
    t.string "name"
    t.integer "vehicle_brand_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vehicle_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "condition"
    t.string "carMaker"
    t.string "carModel"
    t.string "carType"
    t.string "price"
    t.string "gasType"
    t.string "featureImag"
    t.string "galleryImag"
    t.string "internalColor"
    t.string "externalColor"
    t.string "transmision"
    t.string "carYear"
    t.string "carMileage"
    t.string "carDoor"
    t.string "engineType"
    t.string "VIM"
    t.integer "previewOwner"
    t.string "mapLocation"
    t.integer "seatsLine"
    t.string "seatsMaterial"
    t.boolean "airbag"
    t.boolean "sportSeats"
    t.boolean "electricalChairs"
    t.boolean "supportFrontArms"
    t.boolean "safetyBelt"
    t.boolean "multifunctionGuide"
    t.boolean "ac"
    t.boolean "radio"
    t.boolean "DVD"
    t.boolean "cdPlayer"
    t.boolean "cdBox"
    t.boolean "usbinterface"
    t.boolean "mp3Aux"
    t.boolean "speakrsHifi"
    t.boolean "profecionalSound"
    t.boolean "wifi"
    t.boolean "bluethooth"
    t.boolean "trailerCouplig"
    t.boolean "rearSpoiler"
    t.boolean "halogenXenon"
    t.boolean "fogLigth"
    t.boolean "daytimeRunningLigths"
    t.boolean "cleanRearGlasses"
    t.boolean "electricMirrors"
    t.boolean "electricDoors"
    t.boolean "newRubbers"
    t.boolean "factoryRings"
    t.boolean "factoryPaints"
    t.boolean "panoramicCeiling"
    t.boolean "sunroof"
    t.string "version"
    t.boolean "absBrakes"
    t.boolean "alarm"
    t.boolean "reverseCamera"
    t.boolean "rainSensor"
    t.boolean "parkingSensor"
    t.boolean "gps"
    t.boolean "smartKey"
    t.boolean "creuiseControl"
    t.boolean "four_x_four"
    t.boolean "turbo"
    t.boolean "fullPower"
    t.boolean "ecoSystem"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "ads", "ads_subcategories"
  add_foreign_key "ads", "users"
  add_foreign_key "ads_subcategories", "categories"
end
